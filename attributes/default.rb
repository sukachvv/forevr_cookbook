default['user'] = 'forevr'
default['host'] = 'forevr.app'
default['swap_size'] = 1024
default['base_dir'] = '/opt/forevr/'
default['application'] = {
    :src => 'https://github.com/SukachVitally/symfony_pr.git',
    :branch => 'master',
    :parameters => {
        :database_host => '127.0.0.1',
        :database_port => 5432,
        :database_name => 'symfony',
        :database_user => 'homestead',
        :database_password => 'secret'
    }
}
