#
# Cookbook Name:: forevr
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

include_recipe 'forevr::swap'
include_recipe 'forevr::packages_install'
include_recipe 'forevr::utils'
include_recipe 'forevr::composer_install'
include_recipe 'forevr::nginx_configure'
include_recipe 'forevr::project_structure_create'
include_recipe 'forevr::deploy'
