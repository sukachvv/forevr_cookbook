#
# Cookbook Name:: forevr
# Recipe:: project_structure_create
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

Chef::Log.info("Start project structure creating")

group node[:user] do
  action :create
end

user node[:user] do
  home "/home/#{node[:user]}"
  group node[:user]
end

directory node[:base_dir] do
    owner node[:user]
    group node[:user]
    mode '0777'
    action :create
end

directory "#{node[:base_dir]}logs" do
    owner node[:user]
    group node[:user]
    mode '0777'
    action :create
end


