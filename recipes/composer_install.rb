#
# Cookbook Name:: forevr
# Recipe:: composer_install
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

Chef::Log.info("Start composer install")

execute 'install composer' do
  command 'curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer'
  creates '/usr/local/bin/composer'
end