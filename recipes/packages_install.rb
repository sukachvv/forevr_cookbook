#
# Cookbook Name:: forevr
# Recipe:: packages_install
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

Chef::Log.info("Start base packages install")

Chef::Log.info("Start packages install")

include_recipe 'build-essential::default'
include_recipe 'forevr::system_deps_centos'

package 'git'
package 'php70w'
package 'php70w-common'
package 'php70w-opcache'
package 'php70w-fpm'
package 'php70w-xml'
package 'php70w-pdo'
package 'nginx'
