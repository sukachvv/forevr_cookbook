#
# Cookbook Name:: forevr
# Recipe:: nginx_configure
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

Chef::Log.info("Start nginx configure")

execute 'autostart nginx' do
  command 'chkconfig nginx on'
end

execute 'autostart php-fpm' do
  command 'chkconfig php-fpm on'
end

template "/etc/nginx/conf.d/forevr.conf" do
    source 'nginx.conf.erb'
    variables(
        :root => "#{node[:base_dir]}application/web",
        :host => node[:host],
        :error_log => "#{node[:base_dir]}logs/error.log"
    )
end

template  '/etc/php-fpm.d/www.conf' do
    source 'www.conf.rb'
    variables(
        :user => node[:user],
        :group => node[:user]
    )
end
