#
# Cookbook Name:: forevr
# Recipe:: utils
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

Chef::Log.info("Start utils install")

package 'vim'
package 'mc'
package 'htop'