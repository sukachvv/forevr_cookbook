#
# Cookbook Name:: forevr
# Recipe:: deploy
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

Chef::Log.info("Start deploy")

git "#{node[:base_dir]}application" do
  repository node[:application][:src]
  revision node[:application][:branch]
  group node[:user]
  user node[:user]
  action :sync
end

node[:application][:parameters]

template "#{node[:base_dir]}application/app/config/parameters.yml" do
    source 'parameters.yml.rb'
    group node[:user]
    owner node[:user]
    variables(
        :parameters => node[:application][:parameters]
    )
end

execute 'update project' do
  user node[:user]
  group node[:user]
  cwd "#{node[:base_dir]}application"
  command 'composer update'
end

service 'nginx' do
  action :restart
end

service 'php-fpm' do
  action :restart
end
