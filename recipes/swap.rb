#
# Cookbook Name:: forevr
# Recipe:: swap
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

Chef::Log.info("Start create swap")

swap_file '/mnt/swap' do
  size      node[:swap_size]    # MBs
end
