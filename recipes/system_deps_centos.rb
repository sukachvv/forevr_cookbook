#
# Cookbook Name:: forevr
# Recipe:: system_deps_centos
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

Chef::Log.info("Start deps packages install")

package 'epel-release'

#rpm_package 'webtatic-release' do
#  source 'https://mirror.webtatic.com/yum/el7/webtatic-release.rpm'
#  source 'https://mirror.webtatic.com/yum/el6/latest.rpm'
#  action :upgrade
#end

execute 'install webtatic-release' do
  command 'rpm -ivh https://mirror.webtatic.com/yum/el6/latest.rpm'
  not_if { File.exist?('/etc/yum.repos.d/webtatic.repo') }
end
