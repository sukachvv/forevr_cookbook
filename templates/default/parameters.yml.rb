parameters:
    database_host: <%=@parameters[:database_host] %>
    database_port: <%=@parameters[:database_port] %>
    database_name: <%=@parameters[:database_name] %>
    database_user: <%=@parameters[:database_user] %>
    database_password: <%=@parameters[:database_password] %>
    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: null
    mailer_password: null
    secret: ThisTokenIsNotSoSecretChangeIt
